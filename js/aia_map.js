(function ($, Drupal, drupalSettings) {

  // Used by the aia_map view plugin module to display results on a world map.
  Drupal.behaviors.aiaMap = {
    attach: function (context, settings) {
      // Make sue we have settings.
      if (drupalSettings['aia_map']) {
        $(context).find('.aia-map').once('geofield-processed').each(function (index, element) {
          var mapid = $(element).attr('id');

          // Check if the Map container really exists and hasn't been yet initialized.
          if (drupalSettings['aia_map'][mapid] && !Drupal.aiaMap.map_data[mapid]) {
            var map_settings = drupalSettings['aia_map'][mapid]['map_settings'];
            var data = drupalSettings['aia_map'][mapid]['data'];

            // Set the map_data[mapid] settings.
            Drupal.aiaMap.map_data[mapid] = map_settings;
            Drupal.aiaMap.map_initialize(mapid, map_settings, data);
          }
        });
      }
    }
  };

  Drupal.aiaMap = {
    map_data: {},

    // Set up the map.
    map_initialize: function (mapid, map_settings, data) {
      $.noConflict();

      // Code specifically for AnyChart mapping tools.
      anychart.onDocumentReady(function () {

        var map = anychart.map();

        // Turn off the credit on the map (although this didn't work).
        map.credits()
            .enabled(false);

        // Hide the title on the map.
        map.title()
            .enabled(false);

        // Set the default map colour.
        map.unboundRegions().fill(map_settings['default_colour']);
        // Use the world map.
        map.geoData('anychart.maps.world');
        map.interactivity().selectionMode('none');
        map.padding(0);

        var series = map.choropleth(data['features']);

        series.labels(false);
        series.fill(map_settings['active_colour']);

        series.hovered()
            .fill(map_settings['active_colour'])
            .stroke(anychart.color.darken('#f48fb1'));

        map.labels(false);

        // Disable tooltip's title and separator
        map.tooltip()
            .title(false)
            .separator(false);

        // Setup formatter for the tooltip using HTML
        map.tooltip()
            .useHtml(true)
            .format(function () {
              return ''
            });

        // When the cursor moves across countries the title changes. This will
        // trigger the following event and the system will get a new copy of
        // the tooltip html code.
        map.tooltip().onTitleChanged(function () {
          this.contentElement.innerHTML = document.querySelector('.aia-map-hidden-container').innerHTML;
        });

        // This adds the data to the tooltip template which can be found in the
        // aia-map.html.twig file.
        var showCountryData = function (index) {
          var item_data = data['features'][index];
          if (item_data) {
            document.getElementById('tooltip-image').setAttribute("src", map_settings['ihg_image']);
            document.getElementById('tooltip-image').setAttribute("alt", item_data['name']);
            document.getElementById('tooltip-title').innerText = item_data['title'];
          }
        };

        // Render tooltip content on 'pointsHover' event.
        map.listen('pointsHover', function (evt) {
          if (!evt.currentPoint.hovered) {
            return;
          }

          showCountryData(evt.currentPoint.index);
        });

        // Create zoom controls.
        var zoomController = anychart.ui.zoom();
        zoomController.render(map);

        // Set a listener for a single click.
        var click_listen = function(e) {
          window.open(e.point.get('url'), '_blank');
        };

        map.listen('pointClick', click_listen);

        // Set container id for the chart.
        map.container(mapid);

        // Initiate chart drawing.
        map.draw();
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
