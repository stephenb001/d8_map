<?php

namespace Drupal\aia_map\Plugin\views\style;

use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\DefaultStyle;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Style plugin to render a View output as a map.
 *
 * @ingroup views_style_plugins
 *
 * Attributes set below end up in the $this->definition[] array.
 *
 * @ViewsStyle(
 *   id = "aia_map",
 *   title = @Translation("AIA Map"),
 *   help = @Translation("Displays a View as an AIA Map."),
 *   display_types = {"normal"},
 *   theme = "aia-map"
 * )
 */
class AiaMapViewStyle extends DefaultStyle implements ContainerFactoryPluginInterface {

  /**
   * Empty Map Options.
   *
   * @var array
   */
  protected $emptyMapOptions = [
    '0' => 'View No Results Behaviour',
    '1' => 'Empty Map Centered at the Default Center',
  ];

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The Entity type property.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The Entity Info service property.
   *
   * @var string
   */
  protected $entityInfo;

  /**
   * The Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The Entity Field manager service property.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Entity Display Repository service property.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplay;

  /**
   * The Link generator Service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $link;

  /**
   * The geoPhpWrapper service.
   *
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geoPhpWrapper;

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Renderer service property.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $renderer;

  /**
   * The list of fields added to the view.
   *
   * @var array
   */
  protected $viewFields = [];

  /**
   * The MapThemer Manager service .
   *
   * @var \Drupal\geofield_map\MapThemerPluginManager
   */
  protected $mapThemerManager;

  /**
   * The MapThemer Manager service .
   *
   * @var \Drupal\geofield_map\MapThemerInterface
   */
  protected $mapThemerPlugin;


  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = FALSE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = FALSE;

  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * Does the style plugin for itself support to add fields to it's output.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * Should field labels be enabled by default.
   *
   * @var bool
   */
  protected $defaultFieldLabels = TRUE;

  /**
   * {@inheritdoc}
   */
  public function evenEmpty() {
    // Render map even if there is no data.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['demo'] = [
      '#markup' => $this->t('This is a test'),
    ];

    $form['default_colour'] = [
      '#type' => 'color',
      '#title' => $this->t('Default country colour'),
      '#default_value' => !empty($this->options['default_colour']) ? $this->options['default_colour'] : '#f9cc17',
    ];

    $form['active_colour'] = [
      '#type' => 'color',
      '#title' => $this->t('Active country colour'),
      '#default_value' =>  '#f9cc17',
      '#default_value' => !empty($this->options['active_colour']) ? $this->options['active_colour'] : '#f9cc17',
    ];

    // Get list of fields for the country code.
    // Might be worth putting a check in here for the
    foreach ($this->displayHandler->getHandlers('field') as $field_id => $handler) {
      $label = $handler->adminLabel() ?: $field_id;
      $view_fields[$field_id] = $label;
    }

    // Map data source.
    $form['country_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Country code'),
      '#description' => $this->t('Which field contains geodata?'),
      '#options' => $view_fields,
      '#default_value' => $this->options['country_code'],
      '#description' => $this->t('Please choose a field that has the 2 character country code'),
      '#required' => TRUE,
    ];

    return;


  }

  /**
   * Renders the View.
   */
  public function render() {

    $map_settings = $this->options;

    $js_settings = [
      'mapid' => Html::getUniqueId("aia_map_view_" . $this->view->id() . '_' . $this->view->current_display),
      'map_settings' => $map_settings,
      'data' => [],
    ];

    $data = [];

    $this->renderFields($this->view->result);

    foreach ($this->view->result as $id => $result) {
      $link = $this->getFieldValue($id, 'field_external_link');

      $data[$id]['id'] = $this->getFieldValue($id, $map_settings['country_code']);
      $data[$id]['value'] = 1;
      $data[$id]['title'] = $this->getFieldValue($id, 'title');
      $data[$id]['url'] = !empty($link) ? Url::fromUri($link)->toString() : '';
    }

    $js_settings['data'] = [
      'type' => 'FeatureCollection',
      'features' => $data,
    ];

    $element = aia_map_render($js_settings);

    return $element;
  }

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['active_colour'] = ['default' => '#ee2244'];
    $options['default_colour'] = ['default' => '#ee2244'];
    $options['country_code'] = ['default' => ''];
    $options['ihg_image'] = ['default' => drupal_get_path('module', 'aia_map') . '/image/ihg.jpg'];

    return $options;
  }

  /**
   * Get the defined list of Fields added to the View.
   *
   * @return array
   *   The list of Fields names.
   */
  public function getViewFields() {
    return $this->viewFields;
  }

  /**
   * Get View Entity Type.
   *
   * @return string
   *   The entity type name.
   */
  public function getViewEntityType() {
    return $this->entityType;
  }

  /**
   * Returns the Entity Field manager service property.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   The Entity Field manager service property.
   */
  public function getEntityFieldManager() {
    return $this->entityFieldManager;
  }

  /**
   * Get the bundles defined as View Filter.
   */
  public function getViewFilteredBundles() {

    // Get the Views filters.
    $views_filters = $this->view->display_handler->getOption('filters');
    // Set the specific filtered entity types/bundles.
    if (!empty($views_filters['type'])) {
      $bundles = array_keys($views_filters['type']['value']);
    }

    return isset($bundles) ? $bundles : [];

  }

}
